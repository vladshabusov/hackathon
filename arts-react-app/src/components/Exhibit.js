import React from 'react';
import * as Api from 'typescript-fetch-api'
import { withRouter } from "react-router";
import '../App.css';

const api = new Api.DefaultApi()

class Exhibit extends React.Component {

    constructor(props) {
        super(props);
        const id = '00001';

        this.state = {
            exhibit: [],
            id: id
        };

        this.handleReload = this.handleReload.bind(this);
        this.handleReload();
    }


        async handleReload(exhibit) {
            const response = await api.exhibit({ id: this.state.id });
            this.setState({ exhibit: response });
        }


    render() {
        return <div class="filter">
           <div class="catalog_osnova">
                {this.state.exhibit.map(
                    (exhibit) =>
                    <div>
                        <div align="center"class="one_book" >
                            <img src={exhibit.image} width="850px" height="480px" alt="book" /><br />
                            <div class ="one_book_info__1">
                            <span class="book__name">{exhibit.name}, {exhibit.year}</span><span class="book_dop_infa">{exhibit.id} views</span><br />
                            <span class="book__author">{exhibit.author}</span><br />
                            <div class="book_descriptions">{exhibit.descriptions}</div><br />
                            </div>
                        </div>
                    </div>)}
            </div>
        </div>
    }
}

export default withRouter(Exhibit);