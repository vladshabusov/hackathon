import React from 'react';
import * as Api from 'typescript-fetch-api'
import { withRouter } from "react-router";
import '../App.css';

const api = new Api.DefaultApi()

class Sculptures extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            sculptures: []
        };

        this.handleReload = this.handleReload.bind(this);
        this.handleReload();
    }


        async handleReload(sculptures) {
        const response = await api.sculptures({});
        this.setState({ sculptures: response });
     }


    render() {
        return <div class="filter">
            <h2>Catalog of Sculptures</h2>
            <div class="catalog_osnova">
                {this.state.sculptures.map(
                    (sculpture) =>
                    <div>
                       <div align="center" class="book" >
                            <a href="/exhibit/:id"><img src={sculpture.image} width="290px" height="290px" alt="book" /></a><br />
                        </div>
                    </div>)}
            </div>
        </div>
    }
}

export default withRouter(Sculptures);