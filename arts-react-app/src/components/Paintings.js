import React from 'react';
import * as Api from 'typescript-fetch-api'
import { withRouter } from "react-router";
import '../App.css';

const api = new Api.DefaultApi()

class Paintings extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            paintings: []
        };

        this.handleReload = this.handleReload.bind(this);
        this.handleReload();
    }


        async handleReload(paintings) {
        const response = await api.paintings({});
        this.setState({ paintings: response });
     }


    render() {
        return <div class="filter">
            <h2>Catalog of paintings</h2>
            <div class="catalog_osnova">
                {this.state.paintings.map(
                    (painting) =>
                    <div>
                       <div align="center" class="book" >
                            <a href="/exhibit/:id"><img src={painting.image} width="290px" height="290px" alt="book" /></a><br />
                        </div>
                    </div>)}
            </div>
        </div>
    }
}

export default withRouter(Paintings);