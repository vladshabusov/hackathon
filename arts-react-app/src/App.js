import './App.css';
import React from 'react';
import Exhibit from './components/Exhibit';
import Paintings from './components/Paintings';
import Sculptures from './components/Sculptures';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect
} from "react-router-dom";

function App() {
  return (

<Router>
    <div>
        <nav>
          <ul>
            <li>
              <Link to="/home">Home</Link>
            </li>
            <li>
              <Link to="/paintings">Paintings</Link>
            </li>
            <li>
              <Link to="/sculptures">Sculptures</Link>
            </li>
            <li>
              <Link to="/about">About us</Link>
            </li>
            <li>
              <Link to="/exhibit/:id">Random exhibit</Link>
            </li>
          </ul>
        </nav>

        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
      <div className="App">
         
        <section> 
          <Switch>
            <Route path="/about">
              <About />
            </Route>
            <Route path="/paintings">
              <Paintings />
            </Route>
            <Route path="/sculptures">
              <Sculptures />
            </Route>
            <Route path="/exhibit/:id">
              <Exhibit/>
            </Route>
            
            <Route path="/home">
              <Home />              
            </Route>

            <Route path="*">
              <Redirect to={`/home`} />
            </Route>
          </Switch>
        </section>
      </div>
    </div>
    </Router>
  );
}

function About() {
  return (
    <div>
      <h2>
        About us
      </h2>
      <p>Here is the description of the service and necessary terms.</p>
    </div>
  );
}

function Home() {
  return (
    <div>
      <img src="/images/images.png" class="logo" />
			<h1 class="logo_tekst">Modern Arts Museum</h1>
			<div>
			<h2>About us</h2>
			<p> Online Museum of Contemporary Art, where you can visit various exhibitions, get acquainted and learn more about works of modern art without leaving your home.
Here you can see the unusual works of the youngest and most developing artists, sculptors. See how various creative genres are evolving. We started working in this form at the beginning of last year, when the quarantine began.</p>
			<a href="/paintings"><button>Paintings</button></a>
			<a href="/sculptures"> <button>Sculpture</button></a>
		</div>
    </div>
  );
}

export default App;